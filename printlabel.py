#!/usr/bin/python
import sys
import logging
import socket
### This tool is for sending dating to our toshiba labelprinters

logger = logging.getLogger('labelcreater')
logger.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(ch)

def printhelp():
 print """This tool will create labels based on a CSV file.
 
 Paramters: IP LabelList [Label Copies]
 
 First parameter is the ip address of the labelprinter, second is the csv file. The third field is optional and gives the amount of labelcopies. The CSV file has to have 4 fields, separated by ; - Each field may have up to 18 characters and will be its own line.
 Empty fields are valid, but must exist."""
 sys.exit(0)
def parse_csv(file):
    labellist = []
    filehandler = open(file, "ro")
    logger.debug("Created filehandler: %s",filehandler)
    lines=filehandler.readlines()
    filehandler.close()
    logger.debug("Read %s labels", len(lines))
    linecount = 1
    for line in lines:
        logger.debug("Parsing line: %s", linecount)
        line = line.replace('\n','')
        line = line.split(';')
        line.remove('')
        if len(line) != 4:
            logger.error("Line %s has not exact 4 fields, aborting!", line)
            sys.exit(1)
        for field in line:
            if len(field) > 18:
                logger.error("A field in line %s exceeds the 18 character limit, aborting!", linecount)
                sys.exit(1)
        labellist.append(line)
        linecount = linecount+1
    return labellist
        
def create_label_code(labellines, count):
    
    raw_out_data = ''
    template = """{D0528,0597,0508|}
{AY;+10,0|}
{C|}
{PC000;0062,0458,1,1,R,33,B=%L1%|}
{PC001;0102,0458,1,1,R,33,B=%L2%|}
{PC001;0142,0458,1,1,R,33,B=%L3%|}
{PC001;0182,0458,1,1,R,33,B=%L4%|}
{XS;I,%COUNT%,0001C1000|}
"""
    for label in labellines:
        raw_single_label = template.replace('%L1%', label[0])
        raw_single_label = raw_single_label.replace('%L2%', label[1])
        raw_single_label = raw_single_label.replace('%L3%', label[2])
        raw_single_label = raw_single_label.replace('%L4%', label[3])
        raw_single_label = raw_single_label.replace('%COUNT%', count.zfill(4))
        logger.debug("Created label raw data: %s", raw_single_label)
        raw_out_data = raw_out_data + raw_single_label
    return raw_out_data
    
def send_data_to_printer(data,ip):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ip, 8000))
    logger.debug("Got this socket: %s", s)
    s.send(data)
    s.close()

        


def runlabelcreation(arguments):
    printerip = arguments[1]
    csvfile = arguments[2]
    count = arguments[3]
    logger.debug("PrinterIP: %s, CSV-File: %s, Count: %s",printerip,csvfile, count)
    labellines = parse_csv(csvfile)
    logger.debug("Parsed the following labels: %s", labellines)
    rawlabeldata = create_label_code(labellines, count)
    logger.debug("Got a TCPL stream ready for transmission: %s", rawlabeldata)
    send_data_to_printer(rawlabeldata, printerip)
    sys.exit(0)

if __name__ == "__main__":
 if len(sys.argv) == 3:
    sys.argv.append('1')
    runlabelcreation(sys.argv)
 if len(sys.argv) == 4:
    runlabelcreation(sys.argv)   
 printhelp()
  
  
